extern crate sunrise_lite;

#[test]
fn test_sunrise() {
    assert_eq!(
        sunrise_lite::sunrise_sunset(0., 0., 1970, 1, 1),
        (21594, 65227)
    );
}
