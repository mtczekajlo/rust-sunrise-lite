/// Calculates the Julian day for the local true solar transit.
pub fn solar_transit(day: f64, solar_anomaly: f64, ecliptic_longitude: f64) -> f64 {
    day + (0.0053 * libm::sin(solar_anomaly * ::DEGREE)
        - 0.0069 * libm::sin(2. * ecliptic_longitude * ::DEGREE))
}

#[cfg(test)]
mod tests {
    use approx::assert_relative_eq;

    #[test]
    fn test_prime_meridian() {
        assert_relative_eq!(
            super::solar_transit(2440588., 358.30683, 281.08372),
            2440588.00245,
            epsilon = 0.00001
        )
    }
}
