use anomaly::solar_mean_anomaly;
use center::equation_of_center;
use declination::declination;
use hourangle::hour_angle;
use julian::julian_to_unix;
use longitude::ecliptic_longitude;
use noon::mean_solar_noon;
use transit::solar_transit;

/// Calculates the sunrise and sunset times for the given location and date.
pub fn sunrise_sunset(
    latitude: f64,
    longitude: f64,
    year: i32,
    month: u32,
    day: u32,
) -> (i64, i64) {
    let day: f64 = mean_solar_noon(longitude, year, month, day);
    let solar_anomaly: f64 = solar_mean_anomaly(day);
    let equation_of_center: f64 = equation_of_center(solar_anomaly);
    let ecliptic_longitude: f64 = ecliptic_longitude(solar_anomaly, equation_of_center, day);
    let solar_transit: f64 = solar_transit(day, solar_anomaly, ecliptic_longitude);
    let declination: f64 = declination(ecliptic_longitude);
    let hour_angle: f64 = hour_angle(latitude, declination);
    let frac: f64 = hour_angle / 360.;
    (
        julian_to_unix(solar_transit - frac),
        julian_to_unix(solar_transit + frac),
    )
}

#[cfg(test)]
mod tests {
    #[test]
    fn test_prime_meridian() {
        assert_eq!(super::sunrise_sunset(0., 0., 1970, 1, 1), (21594, 65227),)
    }
}
