/// Calculates the second of the two angles required to locate a point on the
/// celestial sphere in the equatorial coordinate system.
pub fn hour_angle(latitude: f64, declination: f64) -> f64 {
    let latitude_rad = latitude * ::DEGREE;
    let declination_rad = declination * ::DEGREE;
    let numerator = -0.01449 - libm::sin(latitude_rad) * libm::sin(declination_rad);
    let denominator = libm::cos(latitude_rad) * libm::cos(declination_rad);
    libm::acos(numerator / denominator) / ::DEGREE
}

#[cfg(test)]
mod tests {
    use approx::assert_relative_eq;

    #[test]
    fn test_prime_meridian() {
        assert_relative_eq!(super::hour_angle(0., -22.97753), 90.9018, epsilon = 0.00001)
    }
}
