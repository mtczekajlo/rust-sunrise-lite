/// Declination calculates one of the two angles required to locate a point on
/// the celestial sphere in the equatorial coordinate system. The ecliptic
/// longitude parameter must be in degrees.
pub fn declination(ecliptic_longitude: f64) -> f64 {
    libm::asin(libm::sin(ecliptic_longitude * ::DEGREE) * 0.39779) / ::DEGREE
}

#[cfg(test)]
mod tests {
    use approx::assert_relative_eq;

    #[test]
    fn test_prime_meridian() {
        assert_relative_eq!(super::declination(281.08372), -22.97753, epsilon = 0.00001)
    }
}
