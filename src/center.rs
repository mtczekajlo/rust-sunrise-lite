/// Calculates the angular difference between the position of the earth in its
/// elliptical orbit and the position it would occupy in a circular orbit for
/// the given mean anomaly.
pub fn equation_of_center(solar_anomaly: f64) -> f64 {
    let anomaly_in_rad: f64 = solar_anomaly * (::PI / 180.);
    let anomaly_sin = libm::sin(anomaly_in_rad);
    let anomaly_2_sin = libm::sin(2. * anomaly_in_rad);
    let anomaly_3_sin = libm::sin(3. * anomaly_in_rad);
    1.9148 * anomaly_sin + 0.02 * anomaly_2_sin + 0.0003 * anomaly_3_sin
}

#[cfg(test)]
mod tests {
    use approx::assert_relative_eq;

    #[test]
    fn test_prime_meridian() {
        assert_relative_eq!(
            super::equation_of_center(358.30683),
            -0.05778,
            epsilon = 0.00001
        )
    }
}
