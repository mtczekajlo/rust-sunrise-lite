#![no_std]

extern crate chrono;

#[cfg(test)]
extern crate approx;

const PI: f64 = 3.14159265358979323846264338327950288_f64;
const DEGREE: f64 = PI / 180.;

mod anomaly;
mod center;
mod declination;
mod hourangle;
mod julian;
mod longitude;
mod noon;
mod perihelion;
mod sunrise;
mod transit;

pub use sunrise::sunrise_sunset;
