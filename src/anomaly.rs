const J2000: f64 = 2451545.;

/// Calculates the angle of the sun relative to the earth for the specified
/// Julian day.
pub fn solar_mean_anomaly(day: f64) -> f64 {
    let v: f64 = (357.5291 + 0.98560028 * (day - J2000)) % 360.;
    if v < 0. {
        v + 360.
    } else {
        v
    }
}

#[cfg(test)]
mod tests {
    use approx::assert_relative_eq;

    #[test]
    fn test_prime_meridian() {
        assert_relative_eq!(
            super::solar_mean_anomaly(2440588.),
            358.30683,
            epsilon = 0.00001
        )
    }
}
