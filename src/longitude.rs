use perihelion;

/// Calculates the angular distance of the earth along the ecliptic.
pub fn ecliptic_longitude(solar_anomaly: f64, equation_of_center: f64, day: f64) -> f64 {
    (solar_anomaly
        + equation_of_center
        + 180.
        + perihelion::argument_of_perihelion(day) % 360.
        + 360.)
        % 360.
}

#[cfg(test)]
mod tests {
    use approx::assert_relative_eq;

    #[test]
    fn test_prime_meridian() {
        assert_relative_eq!(
            super::ecliptic_longitude(358.30683, -0.05778, 2440588.),
            281.08372,
            epsilon = 0.00001
        )
    }
}
